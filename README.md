#UMSocialAne


###AndroidManifest.xml中添加以下权限


		<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />  <!-- 检测网络状态 -->
		<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />     <!-- 获取mac地址作为用户的备用唯一标识 -->
		<uses-permission android:name="android.permission.READ_PHONE_STATE" />      <!-- 获取用户手机的IMEI，用来唯一的标识用户。 -->
		<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" /><!-- 缓存资源优先存入SDcard -->
		<uses-permission android:name="android.permission.INTERNET" />              <!-- 允许应用程序联网，以便向我们的服务器端发送数据。 -->
		<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />  <!-- 用于评论模块分享位置信息 -->
		<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" /><!-- 用于评论模块分享位置信息 -->


###AndroidManifest.xml中添加以下组件
		
	<!-- ###################注册SDK使用的Activity###################### -->
	<!--分享编辑页-->
	<activity
		android:name="com.umeng.socialize.view.ShareActivity"
		android:configChanges="orientation|keyboard"
		android:launchMode="singleTask"
		android:noHistory="true"
		android:theme="@style/Theme.UMDialog"
		android:windowSoftInputMode="stateVisible|adjustResize" >
	</activity>
	<!--为AIR扩展的Activity-->
	<activity
		android:name="com.mxfan.extension.umsocial.ExShareActivity"
		android:screenOrientation="portrait"
		android:theme="@android:style/Theme.Translucent" >
	</activity>
	<!-- ############ QQ空间和QQ SSO授权的Activity注册 ############ -->
		<activity android:name="com.tencent.tauth.AuthActivity" />
	<!-- ###################添加UmengAppkey###################### -->
	<meta-data
		android:name="UMENG_APPKEY"
		android:value="xxxxxxxxxxxxxxxxxxxxxx" >
	</meta-data>
		
>UMENG_APPKEY 需要修改到网站上申请，并在下面组件中修改

代码调用
---
	import com.mxfan.umsocial.ane.UMController;
	import com.mxfan.umsocial.ane.UMShare;
	protected function onShareClick(event:MouseEvent):void
	{
		var controller:UMController = new UMController();
		var umShare:UMShare = new UMShare();
		umShare.image = "/mnt/sdcard/logo_temp.png";
		umShare.content = "from内容分享";
		umShare.wxAppID = "xxxx";
		umShare.wxContentUrl ="http://www.umeng.com"
		controller.openShare(umShare);
	}

1.image: 分享的图片路径，可以是本地路径也可以是网络URL(必须以http开头)

2.content:分享的文字内容

3.wxAppID:应用在微信平台注册的ID，必须填写，否则无法分享微信

4.wxContentUrl:微信、QQ空间等分享内容跳转的URL地址

